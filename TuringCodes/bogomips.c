#include <stdio.h>
#include <time.h>	//clock() miatt

void delay(unsigned long long loops)
{
	for(unsigned long long i = 0; i < loops; i++){}
};

int main()
{
unsigned long long lps = 1;	//kellően nagy pozitív változó
unsigned long long ticks;

printf("BogoMips calculation in progress...\n");

while((lps <<= 1))	//bitshifting
{
	ticks = clock();	//clock ticks száma mióta a program fut
	delay(lps);		//késleltetés
	ticks = clock() - ticks;
	if(ticks >= CLOCKS_PER_SEC)	//vagyis ha nem volt delay
	{
		lps = (lps / ticks) * CLOCKS_PER_SEC;
		printf("Done - %llu.%02llu BogoMips\n", lps / 500000, (lps / 5000) % 100);
		// %llu - unsigned long long miatt , %02llu a tizedesek miatt
		return 0; 
	}
	
} 
printf("fail\n");
	return -1;
}
